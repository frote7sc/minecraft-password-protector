package com.f7sc.minecraftpasswordprotector.listeners;

import com.f7sc.minecraftpasswordprotector.PasswordProtector;
import com.f7sc.minecraftpasswordprotector.handlers.PlayerHandler;
import com.f7sc.minecraftpasswordprotector.enums.PluginPermissions;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;

public class JoinedPlayerListener implements Listener {
    private final PasswordProtector plugin;


    public JoinedPlayerListener(PasswordProtector plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        new PlayerHandler(plugin, e.getPlayer()).restrictPlayer();
    }

    @EventHandler
    public void onBreak(BlockBreakEvent e) {
        if (!e.getPlayer().hasPermission(PluginPermissions.BLOCK_BREAK.getValue())) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlace(BlockPlaceEvent e) {
        if (!e.getPlayer().hasPermission(PluginPermissions.BLOCK_PLACE.getValue())) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e) {
        if (!e.getPlayer().hasPermission(PluginPermissions.PLAYER_AUTHENTICATED.getValue())) {
            e.setCancelled(true);
        }
    }
}
