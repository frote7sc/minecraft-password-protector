package com.f7sc.minecraftpasswordprotector.enums;

public enum PluginSetting {
    PASSWORD("password", "changeme"),
    PASSWORD_MESSAGE("password-message", "Please enter the server password."),
    PASSWORD_INCORRECT_MESSAGE("password-incorrect-message", "The password entered was incorrect."),
    WELCOME_MESSAGE("welcome-message", "Welcome"),
    TIME_UNTIL_KICK_SECONDS("time-until-kick-seconds", "60"),
    VERIFIED_USERS_LIST("verified-users", "");

    private final String key;
    private final String defaultValue;

    PluginSetting(String key, String defaultValue) {
        this.key = key;
        this.defaultValue = defaultValue;
    }

    public String getKey() {
        return key;
    }

    public String getDefaultValue() {
        return defaultValue;
    }
}
