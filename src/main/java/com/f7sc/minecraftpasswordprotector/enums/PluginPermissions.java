package com.f7sc.minecraftpasswordprotector.enums;

public enum PluginPermissions {
    BLOCK_BREAK("f7sc.block.break"),
    BLOCK_PLACE("f7sc.block.place"),
    PLAYER_AUTHENTICATED("f7sc.player.authenticated");

    private final String value;

    PluginPermissions(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
