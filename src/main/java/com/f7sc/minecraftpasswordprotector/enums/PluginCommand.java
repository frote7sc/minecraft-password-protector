package com.f7sc.minecraftpasswordprotector.enums;

import java.util.Arrays;

public enum PluginCommand {
    PASS("pass"),
    UNKNOWN("");

    final String command;

    PluginCommand(String command) {
        this.command = command;
    }

    public static PluginCommand getForString(String command) {
        return Arrays.stream(PluginCommand.values())
                .filter(x -> x.command.equals(command))
                .findFirst()
                .orElse(UNKNOWN);
    }
}
