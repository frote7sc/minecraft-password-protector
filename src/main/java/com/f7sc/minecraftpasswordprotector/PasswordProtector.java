package com.f7sc.minecraftpasswordprotector;

import com.f7sc.minecraftpasswordprotector.enums.PluginSetting;
import com.f7sc.minecraftpasswordprotector.executors.PasswordExecutor;
import com.f7sc.minecraftpasswordprotector.listeners.JoinedPlayerListener;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Arrays;
import java.util.Objects;

public class PasswordProtector extends JavaPlugin {
    @Override
    public void onEnable() {
        initConfig();
        getLogger().info("PasswordProtector Enabled");
        Objects.requireNonNull(getCommand("pass")).setExecutor(new PasswordExecutor(this));
        getServer().getPluginManager().registerEvents(new JoinedPlayerListener(this), this);
    }

    @Override
    public void onDisable() {
        getLogger().info("PasswordProtector Disabled");
    }

    private void initConfig() {
        saveDefaultConfig();
        Arrays.stream(PluginSetting.values()).forEach(this::setConfigValue);
        saveConfig();
    }

    private void setConfigValue(PluginSetting setting) {
        getConfig().addDefault(setting.getKey(), setting.getDefaultValue());
    }
}
