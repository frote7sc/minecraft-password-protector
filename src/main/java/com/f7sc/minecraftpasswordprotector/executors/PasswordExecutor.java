package com.f7sc.minecraftpasswordprotector.executors;

import com.f7sc.minecraftpasswordprotector.PasswordProtector;
import com.f7sc.minecraftpasswordprotector.handlers.PlayerHandler;
import com.f7sc.minecraftpasswordprotector.enums.PluginCommand;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Objects;

public class PasswordExecutor implements CommandExecutor {
    private final PasswordProtector plugin;

    public PasswordExecutor(PasswordProtector plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (commandSender instanceof Player && PluginCommand.getForString(command.getLabel()) == PluginCommand.PASS) {
            Player player = (Player) commandSender;

            PlayerHandler handler = new PlayerHandler(plugin, player);
            if (verifyPassword(strings[0])) {
                handler.unrestrictPlayer();
                handler.sendWelcomeMessage();
            } else {
                handler.sendIncorrectPasswordMessage();
            }

            return true;
        }

        return false;
    }

    private boolean verifyPassword(String enteredPassword) {
        return Objects.equals(this.plugin.getConfig().getString("password"), enteredPassword);
    }
}
