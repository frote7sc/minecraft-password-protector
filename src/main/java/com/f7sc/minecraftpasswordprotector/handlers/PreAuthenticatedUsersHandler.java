package com.f7sc.minecraftpasswordprotector.handlers;

import com.f7sc.minecraftpasswordprotector.PasswordProtector;
import com.f7sc.minecraftpasswordprotector.enums.PluginSetting;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class PreAuthenticatedUsersHandler {
    private static final String SEPARATOR = ";";
    private static final Object LOCK = new Object();

    public static boolean isPlayerVerified(Player player, PasswordProtector plugin) {
        synchronized (LOCK) {
            return getVerifiedPlayers(plugin).stream().anyMatch(x -> UUID.fromString(x).equals(player.getUniqueId()));
        }
    }

    private static List<String> getVerifiedPlayers(PasswordProtector plugin) {
        String verifiedUsersFromConfigFile = plugin.getConfig().getString(PluginSetting.VERIFIED_USERS_LIST.getKey());

        if (verifiedUsersFromConfigFile == null) {
            return Collections.emptyList();
        }

        return Arrays.stream(verifiedUsersFromConfigFile.split(SEPARATOR))
                .filter(x -> !"".equals(x))
                .collect(Collectors.toList());
    }

    public static void verifyPlayer(Player player, PasswordProtector plugin) {
        synchronized (LOCK) {
            List<String> players = getVerifiedPlayers(plugin);
            players.add(player.getUniqueId().toString());

            plugin.getConfig().set(PluginSetting.VERIFIED_USERS_LIST.getKey(), String.join(SEPARATOR, players));
            plugin.saveConfig();
        }
    }
}
