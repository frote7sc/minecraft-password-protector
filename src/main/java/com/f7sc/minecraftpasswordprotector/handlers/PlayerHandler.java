package com.f7sc.minecraftpasswordprotector.handlers;

import com.f7sc.minecraftpasswordprotector.PasswordProtector;
import com.f7sc.minecraftpasswordprotector.enums.PluginPermissions;
import com.f7sc.minecraftpasswordprotector.enums.PluginSetting;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class PlayerHandler {
    private final PasswordProtector plugin;
    private final Player player;
    private final Map<UUID, PermissionAttachment> permissions = new HashMap<>();

    public PlayerHandler(PasswordProtector plugin, Player player) {
        this.plugin = plugin;
        this.player = player;
        this.permissions.put(player.getUniqueId(), player.addAttachment(plugin));
    }

    public void restrictPlayer() {
        if (PreAuthenticatedUsersHandler.isPlayerVerified(player, plugin)) {
            handleInGameSettings(true);
            return;
        }

        handleInGameSettings(false);
        startCountdown();
        sendInputPasswordMessage();
    }

    public void unrestrictPlayer() {
        handleInGameSettings(true);
        PreAuthenticatedUsersHandler.verifyPlayer(player, plugin);
    }

    private void handleInGameSettings(boolean authenticated) {
        player.setInvulnerable(!authenticated);
        player.setInvisible(!authenticated);
        player.setCollidable(authenticated);
        player.setCanPickupItems(authenticated);
        PermissionAttachment playerPermissions = permissions.get(player.getUniqueId());
        playerPermissions.setPermission(PluginPermissions.BLOCK_BREAK.getValue(), authenticated);
        playerPermissions.setPermission(PluginPermissions.BLOCK_PLACE.getValue(), authenticated);
        playerPermissions.setPermission(PluginPermissions.PLAYER_AUTHENTICATED.getValue(), authenticated);
    }

    private void sendInputPasswordMessage() {
        String message = plugin.getConfig().getString(PluginSetting.PASSWORD_MESSAGE.getKey());

        if (message == null) {
            message = PluginSetting.PASSWORD_MESSAGE.getDefaultValue();
        }

        player.sendMessage(message);
    }

    private void startCountdown() {
        if (player.hasPermission(PluginPermissions.PLAYER_AUTHENTICATED.getValue())) {
            return;
        }

        buildCountdown().runTaskTimer(plugin, 20L, 20L);
    }

    private BukkitRunnable buildCountdown() {
        final int[] remainingTime = {plugin.getConfig().getInt(PluginSetting.TIME_UNTIL_KICK_SECONDS.getKey())};

        return new BukkitRunnable() {
            @Override
            public void run() {
                if (player.hasPermission(PluginPermissions.PLAYER_AUTHENTICATED.getValue())) {
                    cancel();
                    return;
                }

                if (remainingTime[0] > 0) {
                    if (showTimer(remainingTime[0])) {
                        player.sendMessage("Please enter the password in " + remainingTime[0] + " second(s).");
                    }
                    remainingTime[0]--;
                } else {
                    player.kickPlayer("Authentication timeout occurred.");
                }
            }
        };
    }

    private boolean showTimer(int currentVal) {
        return currentVal == plugin.getConfig().getInt(PluginSetting.TIME_UNTIL_KICK_SECONDS.getKey()) ||
                currentVal % 10 == 0 ||
                currentVal < 10;
    }

    public void sendWelcomeMessage() {
        String message = plugin.getConfig().getString(PluginSetting.WELCOME_MESSAGE.getKey());

        if (message == null) {
            message = PluginSetting.WELCOME_MESSAGE.getDefaultValue();
        }

        player.sendMessage(message);
    }

    public void sendIncorrectPasswordMessage() {
        String message = plugin.getConfig().getString(PluginSetting.PASSWORD_INCORRECT_MESSAGE.getKey());

        if (message == null) {
            message = PluginSetting.PASSWORD_INCORRECT_MESSAGE.getDefaultValue();
        }

        player.sendMessage(message);
    }
}
